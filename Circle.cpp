/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
using namespace std;
Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(int r){
	this->r = r; 
}

double Circle::getR(){
	return r;
}
double Circle::getR() const{
	return r;
}
double Circle::calculateCircumference(){
	return PI * r * 2;
}
double Circle::calculateCircumference() const{
	return PI * r * 2;
}
double Circle::calculateArea(){
	return PI * r * r;
}
double Circle::calculateArea() const {
	return PI * r * r;
}
bool Circle::equals(Circle c1)
{
	if (r == c1.r)
	{
		cout << "Equal circles!" << endl;
		return true;
	}
	else
	{
		cout << "Not equal circles!" << endl;
		return false;
	}
}
