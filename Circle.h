/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
#define PI 3.14
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(int);
	double getR();
	double getR() const;
	double calculateCircumference();
	double calculateCircumference() const;
	double calculateArea();
	double calculateArea() const;
	bool equals(Circle);
private:
	double r;
};
#endif /* CIRCLE_H_ */
